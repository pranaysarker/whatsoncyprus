<?php 
/*
    Template Name: HomePage
*/
 ?>
<?php get_header(); ?>
            <section id="copy">
                <!-- katsat -->
        
                <!-- katsat end  -->

                <!-- section one starts -->
                <?php if( !empty( cs_get_option('category_section_one') ) && !empty(cs_get_option('category_section_one_show')) ): ?>
                <div class="homeSectionWhatSay clearfix">
                    <div class="homeSectionHead clearfix">
                        <p class="sectionTitle" style="cursor:pointer;" onclick="window.location.href='<?php echo get_category_link( cs_get_option('category_section_one') ); ?>'"><?php  echo get_cat_name(cs_get_option('category_section_one')); ?></p>
                        <p><a href="<?php echo get_category_link( cs_get_option('category_section_one') ); ?>">Δες τα όλα</a></p>
                    </div>
                    <div class="homeSectionBody clearfix">

                    <?php 
                        $args = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page'    => 3,
                            'cat'       => cs_get_option('category_section_one'),
                        ));
                        $c = 1;
                        while ($args->have_posts()) : $args->the_post(); ?>
                        <div class="postBox" id="hide-<?php echo $c; $c++; ?>">
                            <a class="postBox-image zoomin" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'post-thumbnail', array('width' => '620') ) ?>
                            </a>
                            <div class="postBox-content">
                                <div class="contentTop clearfix">
                                    <!-- <span class="hide_mobile postCategory">
                                    Φαγητό & Ποτό </span> -->
                                    <span class="hide_mobile postCategory">
                                    <?php $categories = get_the_category();
 
                                        if ( ! empty( $categories ) ) {
                                            echo esc_html( $categories[0]->name );   
                                        } ?></span>
                                    <span class="hide_mobile separator"></span>
                                    <span class="postPublishedDate">
                                        <b>Άνοιξε <?php the_time( 'd/m/Y' ); ?></b>
                                    </span>
                                </div>
                                <div class="postTitle">
                                    <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
                <!-- sectio one end -->
                <!-- section two starts -->
                <?php if( !empty( cs_get_option('category_section_two') ) && !empty(cs_get_option('category_section_two_show')) ): ?>
                <div class="homeSectionWhatSay clearfix">
                    <div class="homeSectionHead clearfix">
                        <p class="sectionTitle" style="cursor:pointer;" onclick="window.location.href='<?php echo get_category_link( cs_get_option('category_section_two') ); ?>'"><?php  echo get_cat_name(cs_get_option('category_section_two')); ?></p>
                        <p><a href="<?php echo get_category_link( cs_get_option('category_section_two') ); ?>">Δες τα όλα</a></p>
                    </div>
                    <div class="homeSectionBody clearfix">

                    <?php 
                        $args = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page'    => 3,
                            'cat'       => cs_get_option('category_section_two'),
                        ));
                        $c = 1;
                        while ($args->have_posts()) : $args->the_post(); ?>
                        <div class="postBox" id="hide-<?php echo $c; $c++; ?>">
                            <a class="postBox-image zoomin" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'post-thumbnail', array('width' => '620') ) ?>
                            </a>
                            <div class="postBox-content">
                                <div class="contentTop clearfix">
                                    <!-- <span class="hide_mobile postCategory">
                                    Φαγητό & Ποτό </span> -->
                                    <span class="hide_mobile postCategory">
                                    <?php $categories = get_the_category();
 
                                        if ( ! empty( $categories ) ) {
                                            echo esc_html( $categories[0]->name );   
                                        } ?></span>
                                    <span class="hide_mobile separator"></span>
                                    <span class="postPublishedDate">
                                        <b>Άνοιξε <?php the_time( 'd/m/Y' ); ?></b>
                                    </span>
                                </div>
                                <div class="postTitle">
                                    <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
                <!-- sectio two end -->
                <!-- section three starts -->
                <?php if( !empty( cs_get_option('category_section_three') ) && !empty(cs_get_option('category_section_three_show')) ): ?>
                <div class="homeSectionWhatSay clearfix">
                    <div class="homeSectionHead clearfix">
                        <p class="sectionTitle" style="cursor:pointer;" onclick="window.location.href='<?php echo get_category_link( cs_get_option('category_section_three') ); ?>'"><?php  echo get_cat_name(cs_get_option('category_section_three')); ?></p>
                        <p><a href="<?php echo get_category_link( cs_get_option('category_section_three') ); ?>">Δες τα όλα</a></p>
                    </div>
                    <div class="homeSectionBody clearfix">

                    <?php 
                        $args = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page'    => 3,
                            'cat'       => cs_get_option('category_section_three'),
                        ));
                        $c = 1;
                        while ($args->have_posts()) : $args->the_post(); ?>
                        <div class="postBox" id="hide-<?php echo $c; $c++; ?>">
                            <a class="postBox-image zoomin" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'post-thumbnail', array('width' => '620') ) ?>
                            </a>
                            <div class="postBox-content">
                                <div class="contentTop clearfix">
                                    <!-- <span class="hide_mobile postCategory">
                                    Φαγητό & Ποτό </span> -->
                                    <span class="hide_mobile postCategory">
                                    <?php $categories = get_the_category();
 
                                        if ( ! empty( $categories ) ) {
                                            echo esc_html( $categories[0]->name );   
                                        } ?></span>
                                    <span class="hide_mobile separator"></span>
                                    <span class="postPublishedDate">
                                        <b>Άνοιξε <?php the_time( 'd/m/Y' ); ?></b>
                                    </span>
                                </div>
                                <div class="postTitle">
                                    <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
                <!-- sectio three end -->
                <!-- section four starts -->
                <?php if( !empty( cs_get_option('category_section_four') ) && !empty(cs_get_option('category_section_four_show')) ): ?>
                <div class="homeSectionWhatSay clearfix">
                    <div class="homeSectionHead clearfix">
                        <p class="sectionTitle" style="cursor:pointer;" onclick="window.location.href='<?php echo get_category_link( cs_get_option('category_section_four') ); ?>'"><?php  echo get_cat_name(cs_get_option('category_section_four')); ?></p>
                        <p><a href="<?php echo get_category_link( cs_get_option('category_section_four') ); ?>">Δες τα όλα</a></p>
                    </div>
                    <div class="homeSectionBody clearfix">

                    <?php 
                        $args = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page'    => 3,
                            'cat'       => cs_get_option('category_section_four'),
                        ));
                        $c = 1;
                        while ($args->have_posts()) : $args->the_post(); ?>
                        <div class="postBox" id="hide-<?php echo $c; $c++; ?>">
                            <a class="postBox-image zoomin" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'post-thumbnail', array('width' => '620') ) ?>
                            </a>
                            <div class="postBox-content">
                                <div class="contentTop clearfix">
                                    <!-- <span class="hide_mobile postCategory">
                                    Φαγητό & Ποτό </span> -->
                                    <span class="hide_mobile postCategory">
                                    <?php $categories = get_the_category();
 
                                        if ( ! empty( $categories ) ) {
                                            echo esc_html( $categories[0]->name );   
                                        } ?></span>
                                    <span class="hide_mobile separator"></span>
                                    <span class="postPublishedDate">
                                        <b>Άνοιξε <?php the_time( 'd/m/Y' ); ?></b>
                                    </span>
                                </div>
                                <div class="postTitle">
                                    <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
                <!-- sectio four end -->
                <!-- section five starts -->
                <?php if( !empty( cs_get_option('category_section_five') ) && !empty(cs_get_option('category_section_five_show')) ): ?>
                <div class="homeSectionWhatSay clearfix">
                    <div class="homeSectionHead clearfix">
                        <p class="sectionTitle" style="cursor:pointer;" onclick="window.location.href='<?php echo get_category_link( cs_get_option('category_section_five') ); ?>'"><?php  echo get_cat_name(cs_get_option('category_section_five')); ?></p>
                        <p><a href="<?php echo get_category_link( cs_get_option('category_section_five') ); ?>">Δες τα όλα</a></p>
                    </div>
                    <div class="homeSectionBody clearfix">

                    <?php 
                        $args = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page'    => 3,
                            'cat'       => cs_get_option('category_section_five'),
                        ));
                        $c = 1;
                        while ($args->have_posts()) : $args->the_post(); ?>
                        <div class="postBox" id="hide-<?php echo $c; $c++; ?>">
                            <a class="postBox-image zoomin" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'post-thumbnail', array('width' => '620') ) ?>
                            </a>
                            <div class="postBox-content">
                                <div class="contentTop clearfix">
                                    <!-- <span class="hide_mobile postCategory">
                                    Φαγητό & Ποτό </span> -->
                                    <span class="hide_mobile postCategory">
                                    <?php $categories = get_the_category();
 
                                        if ( ! empty( $categories ) ) {
                                            echo esc_html( $categories[0]->name );   
                                        } ?></span>
                                    <span class="hide_mobile separator"></span>
                                    <span class="postPublishedDate">
                                        <b>Άνοιξε <?php the_time( 'd/m/Y' ); ?></b>
                                    </span>
                                </div>
                                <div class="postTitle">
                                    <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                    </div>
                </div>
                <?php endif; ?>
                <!-- sectio five end -->
                
                
                
                
                <!-- tall image section starts -->
                <div class="homeSectionYourArea greenline clearfix">

                    <div class="homeSectionHead greenline clearfix">
                        <p class="sectionTitle">Τι νέο υπάρχει στην πόλη σας…</p>
                    </div>

                    <?php if( !empty(cs_get_option('homepage_tag_one'))): ?>
                    <article class="g1_3 anime">
                        <div class="home_image">
                            <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_one') ); ?>">
                                <img width="300" height="218" src="<?php if(!empty(cs_get_option('homepage_tagimage_one'))){
                                    echo cs_get_option('homepage_tagimage_one');
                                } ?>" alt="">
                            </a>
                            <div class="home_title" style="word-wrap: break-word;">
                                <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_one') ); ?>" title="nicosia">
                                    <?php 
                                        $tag = get_tag(cs_get_option('homepage_tag_one')); // <-- your tag ID
                                        echo $tag->name; 
                                    ?>
                                </a>
                            </div>
                        </div>
                    </article>
                    <?php endif; ?>

                    <?php if( !empty(cs_get_option('homepage_tag_two'))): ?>
                    <article class="g1_3 anime">
                        <div class="home_image">
                            <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_two') ); ?>">
                                <img width="300" height="218" src="<?php if(!empty(cs_get_option('homepage_tagimage_two'))){
                                    echo cs_get_option('homepage_tagimage_two');
                                } ?>" alt="">
                            </a>
                            <div class="home_title" style="word-wrap: break-word;">
                                <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_two') ); ?>" title="limassol">
                                    <?php 
                                        $tag = get_tag(cs_get_option('homepage_tag_two')); // <-- your tag ID
                                        echo $tag->name; 
                                    ?>
                                </a>
                            </div>
                        </div>
                    </article>
                    <?php endif; ?>

                    <?php if( !empty(cs_get_option('homepage_tag_three'))): ?>
                    <article class="g1_3 anime">
                        <div class="home_image">
                            <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_three') ); ?>">
                                <img width="300" height="218" src="<?php if(!empty(cs_get_option('homepage_tagimage_three'))){
                                    echo cs_get_option('homepage_tagimage_three');
                                } ?>" alt="">
                            </a>
                            <div class="home_title" style="word-wrap: break-word;">
                                <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_three') ); ?>" title="larnaca">
                                    <?php 
                                        $tag = get_tag(cs_get_option('homepage_tag_three')); // <-- your tag ID
                                        echo $tag->name; 
                                    ?>
                                </a>
                            </div>
                        </div>
                    </article>
                    <?php endif; ?>

                    <?php if( !empty(cs_get_option('homepage_tag_four'))): ?>
                    <article class="g1_3 anime">
                        <div class="home_image">
                            <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_four') ); ?>">
                                <img width="300" height="218" src="<?php if(!empty(cs_get_option('homepage_tagimage_four'))){
                                    echo cs_get_option('homepage_tagimage_four');
                                } ?>" alt="">
                            </a>
                            <div class="home_title" style="word-wrap: break-word;">
                                <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_four') ); ?>" title="paphos">
                                    <?php 
                                        $tag = get_tag(cs_get_option('homepage_tag_four')); // <-- your tag ID
                                        echo $tag->name; 
                                    ?>
                                </a>
                            </div>
                        </div>
                    </article>
                    <?php endif; ?>
                    <?php if( !empty(cs_get_option('homepage_tag_five'))): ?>
                    <article class="g1_3 anime">
                        <div class="home_image">
                            <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_five') ); ?>">
                                <img width="300" height="218" src="<?php if(!empty(cs_get_option('homepage_tagimage_five'))){
                                    echo cs_get_option('homepage_tagimage_five');
                                } ?>" alt="">
                            </a>
                            <div class="home_title" style="word-wrap: break-word;">
                                <a href="<?php echo get_tag_link( cs_get_option('homepage_tag_five') ); ?>" title="famagusta">
                                    <?php 
                                        $tag = get_tag(cs_get_option('homepage_tag_five')); // <-- your tag ID
                                        echo $tag->name; 
                                    ?>
                                </a>
                            </div>
                        </div>
                    </article>
                    <?php endif; ?>
                    
                </div>

                <!-- tall post image section ends -->
            </section>
            
            
            
            <?php get_footer(); ?>