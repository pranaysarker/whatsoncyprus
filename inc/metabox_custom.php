<?php 
add_action( 'cmb2_admin_init', 'cmb2_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 */
function cmb2_sample_metaboxes() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_themes_';

	/**
	 * Initiate the metabox
	 */
	$cmb = new_cmb2_box( array(
		'id'            => 'test_metabox',
		'title'         => __( 'Post Metabox', 'cmb2' ),
		'object_types'  => array( 'post', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );

	// Regular text field
	$cmb->add_field( array(
		'name'       => __( 'Sidebar Category', 'cmb2' ),
		'desc'       => __( 'Sidebar category', 'cmb2' ),
		'id'         => $prefix . 'category',
		'type'       => 'textarea',
		'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
		// 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
		// 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
		// 'on_front'        => false, // Optionally designate a field to wp-admin only
		// 'repeatable'      => true,
	) );

	// URL text field
	$cmb->add_field( array(
		'name' => __( 'City', 'cmb2' ),
		'desc' => __( 'City for sidebar', 'cmb2' ),
		'id'   => $prefix . 'city',
		'type' => 'text',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );

	$cmb->add_field( array(
		'name' => __( 'Address sidebar', 'cmb2' ),
		'desc' => __( 'Sidebar address', 'cmb2' ),
		'id'   => $prefix . 'address',
		'type' => 'textarea',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );

	$cmb->add_field( array(
		'name' => __( 'Phone Number', 'cmb2' ),
		'desc' => __( 'Phone number sidebar', 'cmb2' ),
		'id'   => $prefix . 'phone',
		'type' => 'text',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );
	$cmb->add_field( array(
		'name' => __( 'Working Hours', 'cmb2' ),
		'desc' => __( 'Sidebar Working hours', 'cmb2' ),
		'id'   => $prefix . 'working_hour',
		'type' => 'text',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );

	$cmb->add_field( array(
		'name' => __( 'Website link', 'cmb2' ),
		'desc' => __( 'Sidebar Website link', 'cmb2' ),
		'id'   => $prefix . 'website',
		'type' => 'text_url',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );
	$cmb->add_field( array(
		'name' => __( 'Facebook link', 'cmb2' ),
		'desc' => __( 'Sidebar Facebook link', 'cmb2' ),
		'id'   => $prefix . 'facebook',
		'type' => 'text_url',
		// 'protocols' => array('http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet'), // Array of allowed protocols
		// 'repeatable' => true,
	) );

	// Email text field
	$cmb->add_field( array(
		'name' => __( 'Instagram Link', 'cmb2' ),
		'desc' => __( 'Sidebar Instagram link', 'cmb2' ),
		'id'   => $prefix . 'instagram',
		'type' => 'text_url',
		// 'repeatable' => true,
	) );
	// Video link field
	$cmb->add_field( array(
		'name' => __( 'Video Link', 'cmb2' ),
		'desc' => __( 'Sidebar Video link', 'cmb2' ),
		'id'   => $prefix . 'video_link',
		'type' => 'text_url',
		// 'repeatable' => true,
	) );
	// Video link field
	$cmb->add_field( array(
		'name' => __( 'Twitter Link', 'cmb2' ),
		'desc' => __( 'Sidebar Twitter link', 'cmb2' ),
		'id'   => $prefix . 'twitter',
		'type' => 'text_url',
		// 'repeatable' => true,
	) );

	// Add other metaboxes as needed

}