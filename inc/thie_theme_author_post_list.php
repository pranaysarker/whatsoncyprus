<?php

class thie_theme_author_post_list extends WP_Widget {
		/**
	 * Sets up a new Archives widget instance.
	 *
	 * @since 2.8.0
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'widget_list',
			'description' => __( 'A widget to show my Widget.' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct('theme_list', __('Theme list'), $widget_ops);
	}

	/**
	 * Outputs the content for the current Archives widget instance.
	 *
	 * @since 2.8.0
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Archives widget instance.
	 */
	public function widget( $args, $instance ) { ?>
		<h4 style="font-weight:normal;">
	      Τηλέφωνο: <a style="color:#85af4b;text-decoration:none;" href="tel:<?php echo $instance['phone']; ?>"><?php echo $instance['phone']; ?></a><br>
	      Διεύθυνση: <a style="color:#85af4b;text-decoration:none;" href="#"><?php echo $instance['address']; ?></a><br>
	      Ώρες λειτουργίας: <a style="cursor:default;color:#85af4b;text-decoration:none;" href="javascript:void(0)"><?php echo $instance['work_time']; ?></a><br>
	      Facebook: <a style="color:#85af4b;text-decoration:none;" href="<?php echo $instance['fb_link']; ?>" target="_blank">Visit Facebook</a><br>
      </h4>
	<?php }


	/**
	 * Outputs the settings form for the Archives widget.
	 *
	 * @since 2.8.0
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) { ?>
		<p>
			<label for="<?php echo $this->get_field_name('phone'); ?>">Phone</label>
			<input type="text" name="<?php echo $this->get_field_name('phone'); ?>" value="<?php echo $instance['phone']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_name('address'); ?>">Address</label>
			<input type="text" name="<?php echo $this->get_field_name('address'); ?>" value="<?php echo $instance['address']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_name('work_time'); ?>">Working Hour</label>
			<input type="text" name="<?php echo $this->get_field_name('work_time'); ?>" value="<?php echo $instance['work_time']; ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_name('fb_link'); ?>">Facebook Link</label>
			<input type="text" name="<?php echo $this->get_field_name('fb_link'); ?>" value="<?php echo $instance['fb_link']; ?>">
		</p>
		<?php
	}
}