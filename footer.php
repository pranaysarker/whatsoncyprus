<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package whatsnewcyprus
 */

?>
<script>
    jQuery(document).ready(function(){
      jQuery("#pull").click(function(){
        jQuery("#mob_menu").slideToggle();
      });
    }); 
</script>
	<div class="footerNewsletterWrapper">
                    <div class="newsletter">
                        <p>Εγγραφείτε στο Newsletter για να Μαθαίνετε τα Καινούργια της Αγοράς!</p>
                        <div class="newsletterActionBox">
                            <!-- <input type="email" name="ms-email" id="email" placeholder="Εγγραφή στο newsletter" required />
                            
                            <button type="submit" name="submit" class="signup-button">Εγγραφή</button> -->
                            <?php echo do_shortcode('[newsletter]'); ?>
                        </div>
                    </div>
            </div>
            <footer id="colophon" role="contentinfo">
                <div class="footerLinks">
                    
                    <!-- <a href="meet-us.html">WHAT'S NEW CYPRUS</a>
                    <a href="contact-us.html">ΕΠΙΚΟΙΝΩΝΙΑ</a>
                    <a href="oroi-xrisis.html">ΟΡΟΙ ΧΡΗΣΗΣ</a>
                    <a href="advertisment.html">ΔΙΑΦΗΜΙΣΤΕΙΤΕ</a>
                    
                    <a href="diagonismos-whatsnewcyprus.html">ΔΙΑΓΩΝΙΣΜΟΣ</a> -->
                    <?php wp_nav_menu( array(
                        'theme_location'  => 'footer-menu',
                        'container'       => ' ',
                        'items_wrap'      => '%3$s',
                        'depth'           => 1,
                        'walker'          => new Footer_menu_walker(),
                    ) ); ?>
                </div>
                <a target="_blank" href="<?php if( !empty( cs_get_option('page_top_bottom_facebook') ) ){
                            echo cs_get_option('page_top_bottom_facebook');
                        }else{
                            echo '#';
                        } ?>">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" height>
                </a>
                
                <br><br>
                <span id="lCopyright">
                <?php if(!empty( cs_get_option('copyright_text') ) ){
                    echo cs_get_option('copyright_text');
                } ?>
                <br/><?php if( !empty(cs_get_option('copyright_text_bottom'))){
                    echo cs_get_option('copyright_text_bottom');
                } ?>
                </span>
            </footer>
            <div class="hide_mobile" id="mycontent" style="display:none;z-index:99;position:fixed;bottom:0px;width:100%;background:#000000;color:#fff;text-align:center;padding:40px;">
                <div style="text-align:center;">
                    
                    <form action="https://app.moosend.com/subscribe/996fd411-c2cd-4e02-a9ca-87ab29d3ce4c" method="post" id="ms-sub-form" target="_blank">
                        <span style="font-size:30px;">Για να ενημερώνεστε για ότι καινούργιο εγγραφείτε στο ενημερωτικό μας δελτίο.</span><br/><br/>
                        <div>
                            <label for="email">email:</label><input type="email" name="ms-email" id="email" style="padding: 8px;width: 316px;" required />
                            <input style="padding: 8px;font-size: 14px;" type="submit" value="Εγγραφή" />
                        </form>
                        
                        <div class="clear:both;"></div>
                    </div>
                    <div style="position: fixed;right: 10px;bottom: 136px;">
                        <a class="click-parent-hide" href="#" onclick='return hideTOPaneb();' style="color:#fff;">close [x]</a>
                    </div>
                </div>
            </div>
<script>
            function getSLCookie(cookie_name){
    var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|jQuery)');
    if (results)
        return (decodeURI(results[2]));
    else
        return null;
}

function setSLCookie(name, value, ttl, path, domain, secure) {
    var cookie_string = name + "=" + encodeURI(value);
    if (ttl){
        var today = new Date();
        today.setTime(today.getTime());
        expires = 1000 * 60 * 60 * ttl;
        var expires_date = new Date( today.getTime() + (expires) );
        cookie_string += "; expires=" + expires_date.toGMTString();
    }
    if (path)
        cookie_string += "; path=" + encodeURI(path);
    if (domain)
        cookie_string += "; domain=" + encodeURI(domain);
    if (secure)
        cookie_string += "; secure";
    document.cookie = cookie_string;
}
    
    
function showTOPaneb() {
     if (self.innerHeight) {
          w = self.innerWidth;
     } else if (document.documentElement && document.documentElement.clientHeight) {
          w = document.documentElement.clientWidth;
     } else if (document.body) {
          w = document.body.clientWidth;
     }

     el1 = document.getElementById("mycontent");
     el1.style.visibility       = "visible";
     el1.style.display          = "block";

    clearInterval(intvl);
return;
}   
    
    
function hideTOPaneb() {
     el = document.getElementById("mycontent");
     el.style.display    = "none";
     el.style.visibility = "hidden";
return;
}      

var  hasPlayed = getSLCookie("newsletter");

if (hasPlayed == false || hasPlayed == null) {
    intvl = setInterval("showTOPaneb();",20000);
    setSLCookie("newsletter", "true", 8, "/");

}
//window.onload = showTOPane;
</script>
<script>
            // function handleClick() {
            //     var checkbox1 = jQuery('#checkbox1:checked').val();
            //     var checkbox2 = jQuery('#checkbox2:checked').val();
            //     var checkbox3 = jQuery('#checkbox3:checked').val();
            //     var checkbox4 = jQuery('#checkbox4:checked').val();
            //     var checkbox5 = jQuery('#checkbox5:checked').val();
                
                
            //     if (checkbox1 == 'on') {
            //         var target = jQuery('#checkbox1').attr('data-target');
            //         // jQuery( '.tag-child-one' ).show('slow');
            //         jQuery('.'+target).show();
            //     }
            //     else {
            //         var target = jQuery('#checkbox1').attr('data-target');
            //         // jQuery( '.tag-child-one' ).hide();
            //         jQuery('.'+target).hide();
            //     }
            //     if (checkbox2 == 'on') {
            //         var target = jQuery('#checkbox2').attr('data-target');
            //         // jQuery( '.tag-child-two' ).show('slow');
            //         jQuery('.'+target).show();
            //     }
            //     else {
            //         var target = jQuery('#checkbox2').attr('data-target');
            //         // jQuery( '.tag-child-two' ).hide();
            //         jQuery('.'+target).hide();
            //     }
            //     if (checkbox3 == 'on') {
            //         var target = jQuery('#checkbox3').attr('data-target');
            //         // jQuery( '.tag-child-three' ).show('slow');
            //         jQuery('.'+target).show();
            //     }
            //     else {
            //         var target = jQuery('#checkbox3').attr('data-target');
            //         // jQuery( '.tag-child-three' ).hide();
            //         jQuery('.'+target).hide();
            //     }
            //     if (checkbox4 == 'on') {
            //         var target = jQuery('#checkbox4').attr('data-target');
            //         // jQuery( '.tag-child-four' ).show('slow');
            //         jQuery('.'+target).show();
            //     }
            //     else {
            //         var target = jQuery('#checkbox4').attr('data-target');
            //         // jQuery( '.tag-child-four' ).hide();
            //         jQuery('.'+target).hide();
            //     }
            //     if (checkbox5 == 'on') {
            //         var target = jQuery('#checkbox5').attr('data-target');
            //         // jQuery( '.tag-child-five' ).show('slow');
            //         jQuery('.'+target).show();
            //     }
            //     else {
            //         var target = jQuery('#checkbox5').attr('data-target');
            //         // jQuery( '.tag-child-five' ).hide();
            //         jQuery('.'+target).hide();
            //     }
                
            // }

            // function handleClick() {
            //     var target = $(this).attr('data-target');
            //     if( $(this).attr('data-show') == 'on' ){
            //         $('.'+target).show();
            //         $(this).attr('data-show','off');
            //     }else {
            //         $('.'+target).hide();
            //         $(this).attr('data-show','on')
            //     }
            //     console.log($(this));
            // }


            function hideTOPaneb() {
                 el = document.getElementById("mycontent");
                 el.style.display    = "none";
                 el.style.visibility = "hidden";
                return;
            }
            
            
        </script>
            <?php wp_footer(); ?>
        </body>
    </html>
