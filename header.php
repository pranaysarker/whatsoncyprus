<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package whatsnewcyprus
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width">
        
       <!--  <script type="text/javascript" src="../cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script> -->
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=greek,greek-ext" rel="stylesheet">
        <?php wp_head(); ?>
    </head>
    <body class="home page shortHeader">
        <script>

    jQuery(function () {
        var jQueryrange = jQuery("#range");
        jQueryrange.ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            step: 500,
            force_edges: true,
            grid_num: 12,
            grid: true,
            type: "double",
            values: [
                
                "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος",
                "Νοέμβριος", "Δεκέμβριος", "Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάϊος", "Ιούνιος", "Ιούλιος"
            ],
        });
         
        
        jQueryrange.on("change", function () {
            var jQuerythis = jQuery(this),
                from = jQuerythis.data("from"),
                to = jQuerythis.data("to");
                //alert(from + " - " + to);
                if ((from > '8') || (to < '8')){
                    jQuery(".April").hide();
                }
                else {
                    jQuery(".April").show('slow');
                }
                if ((from > '7') || (to < '7')){
                    jQuery(".March").hide();
                }
                else {
                    jQuery(".March").show('slow');
                }
                if ((from > '6') || (to < '6')) {
                    jQuery(".February").hide();
                }
                else {
                    jQuery(".February").show('slow');
                }
                if ((from > '5') || (to < '5')) {
                    jQuery(".January").hide();
                }
                else {
                    jQuery(".January").show('slow');
                }
                if ((from > '4') || (to < '4')) {
                    jQuery(".December").hide();
                }
                else {
                    jQuery(".December").show('slow');
                }
                if ((from > '3') || (to < '3')) {
                    jQuery(".November").hide();
                }
                else {
                    jQuery(".November").show('slow');
                }
                if ((from > '2') || (to < '2')) {
                    jQuery(".October").hide();
                }
                else {
                    jQuery(".October").show('slow');
                }
                if ((from > '1') || (to < '1')) {
                    jQuery(".September").hide();
                }
                else {
                    jQuery(".September").show('slow');
                }
                if ((from > '0') || (to < '0')) {
                    jQuery(".August").hide();
                }
                else {
                    jQuery(".August").show('slow');
                }
                if ((from > '11') || (to < '11')) {
                    jQuery(".July").hide();
                }
                else {
                    jQuery(".July").show('slow');
                }
                if ((from > '10') || (to < '10')) {
                    jQuery(".June").hide();
                }
                else {
                    jQuery(".June").show('slow');
                }
                if ((from > '9') || (to < '9')) {
                    jQuery(".May").hide();
                }
                else {
                    jQuery(".May").show('slow');
                }
        });
    });
</script>
        <div id="wrapper">
            
            <header id="main">
                <div class="hide_mobile logoBox">
                    <a href="<?php echo home_url(); ?>" class="shortLogo">
                            <img src="<?php 
                            $custom_logo_id = get_theme_mod( 'custom_logo' );
                            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                            echo $image[0]; 
                            ?>" />
                    </a>
                </div>
                <div class="nav-line a">
                    <nav>
                        <div class="menu-main-container">
                            <?php wp_nav_menu( array(
                                'theme_location'  => 'main-menu',
                                'menu_class'      => 'nav-menu',
                                'menu_id'         => 'menu-main',
                                'container'       => ' ',
                            ) ); ?>
                            <!-- <ul id="menu-main" class="nav-menu">
                                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children"><a href="index.html">ΑΡΧΙΚΗ</a></li>
                                
                                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                                    <a href="eat-and-drink/index.html">
                                    ΦΑΓΗΤΟ & ΠΟΤΟ</a>
                                    <ul class="sub-menu">
                                        <li class="first"><a href="eat-and-drink-nicosia/index.html" title="Λευκωσία" >ΛΕΥΚΩΣΙΑ</a></li>
                                        <li><a href="eat-and-drink-limassol/index.html" title="Λεμεσός" >ΛΕΜΕΣΟΣ</a></li>
                                        <li><a href="eat-and-drink-larnaca/index.html" title="Λάρνακα" >ΛΑΡΝΑΚΑ</a></li>
                                        <li><a href="eat-and-drink-paphos/index.html" title="Πάφος" >ΠΑΦΟΣ</a></li>
                                        <li class="last"><a href="eat-and-drink-famagusta/index.html" title="Αμμόχωστος" >ΑΜΜΟΧΩΣΤΟΣ</a></li>
                                    </ul>
                                </li>
                                
                                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                                    <a href="shops/index.html">
                                    ΚΑΤΑΣΤΗΜΑΤΑ</a>
                                    <ul class="sub-menu">
                                        <li class="first"><a href="shops-nicosia/index.html" title="Λευκωσία" >ΛΕΥΚΩΣΙΑ</a></li>
                                        <li><a href="shops-limassol/index.html" title="Λεμεσός" >ΛΕΜΕΣΟΣ</a></li>
                                        <li><a href="shops-larnaca/index.html" title="Λάρνακα" >ΛΑΡΝΑΚΑ</a></li>
                                        <li><a href="shops-famagusta/index.html" title="Αμμόχωστος" >ΑΜΜΟΧΩΣΤΟΣ</a></li>
                                        <li class="last"><a href="shops-pahos/index.html" title="Πάφος" >ΠΑΦΟΣ</a></li>
                                    </ul>
                                </li>
                                
                                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                                    <a href="services/index.html">
                                    ΥΠΗΡΕΣΙΕΣ</a>
                                    <ul class="sub-menu">
                                        <li class="first"><a href="services-nicosia/index.html" title="Λευκωσία" >ΛΕΥΚΩΣΙΑ</a></li>
                                        <li><a href="services-limassol/index.html" title="Λεμεσός" >ΛΕΜΕΣΟΣ</a></li>
                                        <li><a href="services-larnaca/index.html" title="Λάρνακα" >ΛΑΡΝΑΚΑ</a></li>
                                        <li><a href="services-paphos.html" title="Πάφος" >ΠΑΦΟΣ</a></li>
                                        <li><a href="services-famagusta/index.html" title="Αμμόχωστος" >ΑΜΜΟΧΩΣΤΟΣ</a></li>
                                        <li class="last"><a href="pagkypria/index.html" title="Παγκύπρια" >ΠΑΓΚΥΠΡΙΑ</a></li>
                                    </ul>
                                </li>
                                
                                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children"><a href="nea-proionta-and-ypiresies/index.html">ΝΕΑ ΠΡΟΙΟΝΤΑ & ΥΠΗΡΕΣΙΕΣ</a></li>
                                
                                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children"><a href="diagonismos-whatsnewcyprus.html">ΔΙΑΓΩΝΙΣΜΟΣ</a></li>
                                
                                <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children"><a href="exeis-nea-epixeirisi.html">ΔΙΑΦΗΜΙΣΤΕΙΤΕ</a></li>
                                
                            </ul> -->
                            
                        </div>
                        <a href="javascript://" id="pull">
                            <img src="<?php 
                            $custom_logo_id = get_theme_mod( 'custom_logo' );
                            $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                            echo $image[0]; 
                            ?>" />
                        </a>
                    </nav>
                    <div id="mob_menu">
                        <div class="menu-mobile-container">

                            <?php wp_nav_menu( array(
                                'theme_location'  => 'main-menu',
                                'menu_class'      => 'nav-menu',
                                'menu_id'         => 'menu-main',
                                'container_class'   => 'menu-mobile'
                            ) ); ?>
                            <!-- <ul id="menu-mobile">
                                <ul id="menu-main" class="nav-menu">
                                    <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children"><a href="index.html">ΑΡΧΙΚΗ</a></li>
                                    
                                    <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                                        <a href="eat-and-drink/index.html">
                                        ΦΑΓΗΤΟ & ΠΟΤΟ</a>
                                        <ul class="sub-menu">
                                            <li class="first"><a href="eat-and-drink-nicosia/index.html" title="Λευκωσία" >ΛΕΥΚΩΣΙΑ</a></li>
                                            <li><a href="eat-and-drink-limassol/index.html" title="Λεμεσός" >ΛΕΜΕΣΟΣ</a></li>
                                            <li><a href="eat-and-drink-larnaca/index.html" title="Λάρνακα" >ΛΑΡΝΑΚΑ</a></li>
                                            <li><a href="eat-and-drink-paphos/index.html" title="Πάφος" >ΠΑΦΟΣ</a></li>
                                            <li class="last"><a href="eat-and-drink-famagusta/index.html" title="Αμμόχωστος" >ΑΜΜΟΧΩΣΤΟΣ</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                                        <a href="shops/index.html">
                                        ΚΑΤΑΣΤΗΜΑΤΑ</a>
                                        <ul class="sub-menu">
                                            <li class="first"><a href="shops-nicosia/index.html" title="Λευκωσία" >ΛΕΥΚΩΣΙΑ</a></li>
                                            <li><a href="shops-limassol/index.html" title="Λεμεσός" >ΛΕΜΕΣΟΣ</a></li>
                                            <li><a href="shops-larnaca/index.html" title="Λάρνακα" >ΛΑΡΝΑΚΑ</a></li>
                                            <li><a href="shops-famagusta/index.html" title="Αμμόχωστος" >ΑΜΜΟΧΩΣΤΟΣ</a></li>
                                            <li class="last"><a href="shops-pahos/index.html" title="Πάφος" >ΠΑΦΟΣ</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children">
                                        <a href="services/index.html">
                                        ΥΠΗΡΕΣΙΕΣ</a>
                                        <ul class="sub-menu">
                                            <li class="first"><a href="services-nicosia/index.html" title="Λευκωσία" >ΛΕΥΚΩΣΙΑ</a></li>
                                            <li><a href="services-limassol/index.html" title="Λεμεσός" >ΛΕΜΕΣΟΣ</a></li>
                                            <li><a href="services-larnaca/index.html" title="Λάρνακα" >ΛΑΡΝΑΚΑ</a></li>
                                            <li><a href="services-paphos.html" title="Πάφος" >ΠΑΦΟΣ</a></li>
                                            <li><a href="services-famagusta/index.html" title="Αμμόχωστος" >ΑΜΜΟΧΩΣΤΟΣ</a></li>
                                            <li class="last"><a href="pagkypria/index.html" title="Παγκύπρια" >ΠΑΓΚΥΠΡΙΑ</a></li>
                                        </ul>
                                    </li>
                                    
                                    <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children"><a href="nea-proionta-and-ypiresies/index.html">ΝΕΑ ΠΡΟΙΟΝΤΑ & ΥΠΗΡΕΣΙΕΣ</a></li>
                                    
                                    <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children"><a href="diagonismos-whatsnewcyprus.html">ΔΙΑΓΩΝΙΣΜΟΣ</a></li>
                                    
                                    <li class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-has-children"><a href="exeis-nea-epixeirisi.html">ΔΙΑΦΗΜΙΣΤΕΙΤΕ</a></li>
                                    
                                </ul>
                            </ul> -->
                        </div>
                    </div>
                    <div id="social">
                        <a target="_blank" href="<?php if( !empty( cs_get_option('page_top_bottom_facebook') ) ){
                            echo cs_get_option('page_top_bottom_facebook');
                        }else{
                            echo '#';
                        } ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" height="25px">
                        </a>
                        
                    </div>
                </div>
            </header>
            
            <div class="clearfix" style="display: none;"></div>
            <br/>
