<?php
/*
Template Name: ContactPage
*/
?>
<?php get_header(); ?>
<section id="copy" style="max-width:1150px;margin:auto;">
    <article id="listing" style="    width: 100%!important;padding: 10px;margin: 0px!important;">
        
        <h1><?php wp_title(''); ?></h1>
        <div class="wpEditorContent myeditor" style="clear:left;">
            <p>
            </p><p><style>
            input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            }
            input[type=email], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            }
            input[type=submit] {
            width: 100%;
            background-color: #85af4b;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            }
            input[type=submit]:hover {
            background-color: #000000;
            }
            textarea
            {
            border:1px solid #999999;
            width:100%;
            margin:5px 0;
            padding:3px;
            max-height: 100px;
            }
            </style>
        </p>
        <div>
            <?php if( !empty(cs_get_option('contact_page_content_top')) ){
                echo cs_get_option('contact_page_content_top');
            } ?>
            
            
            <?php
            if( !empty(cs_get_option('contact_form_shortCode')) ){
                echo do_shortcode( cs_get_option('contact_form_shortCode') );
            }
            ?>
            
            
            <!-- /.contact-form -->
        </div>
        <br>
    </div>
    
    <br>
    <div style="clear:both;"></div>
</article>
</section>



<?php get_footer(); ?>