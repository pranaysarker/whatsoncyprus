<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package whatsnewcyprus
 */

get_header();
?>
    <section id="copy">
                <div class="homeSectionWhatSay clearfix" style="padding-top:0px;">
                    <div class="homeSectionHead clearfix" style="float:left;">
                        <h2 class="sectionTitle2"><?php wp_title(''); ?></h2>
                    </div>
                    <div id="show_nc"><div style="clear:both;"></div></div>

                    <!-- post type tag filter area here -->
                    <?php 
                    $tag = get_queried_object();
                    $tag->slug;
                    if( $tag->taxonomy === "post_tag" ): ?>
                    <?php
                        $showCategoryClass = '';
                        $categories=get_categories();
                        $cc = 1;
                        if( !empty($categories) ):

                            foreach ($categories as $c) :
                            // var_dump($c);
                            // what you really want instead of var_dump is something to
                            // to create markup-- list items maybe, For example...
                                    $category = $c->cat_name;
                                    $showCategoryClass = "category-".$c->term_id;
                                    $showCategoryClassTwo = "category-".$c->slug;

                                if(!$c->parent && $c->term_id !== 1):
                            ?>
                        <div class="toggle_out first" <?php if( $cc === 1 ){ echo 'id="toggle_first"'; } ?>>
                            <div id="toggles">
                                <!-- <input onclick="handleClick(this);" name="checkbox1" id="checkbox1" class="ios-toggle" checked="" type="checkbox">
                                <label for="checkbox1" class="checkbox-label" data-off="Λευκωσία" data-on="Λευκωσία"></label> -->

                                <input type="checkbox" name="checkbox<?php echo $cc; ?>" id="checkbox<?php echo $cc; ?>" class="ios-toggle" data-target="<?php echo $showCategoryClass; ?>" data-targettwo="<?php echo $showCategoryClassTwo; ?>" checked />

                                <label for="checkbox<?php echo $cc; ?>" class="checkbox-label check-box-change" data-off="<?php echo $category; ?>" data-on="<?php echo $category; ?>" data-target="<?php echo $showCategoryClass; ?>"></label>
                            </div>
                        </div>
                    <?php 
                        endif;
                            $cc++;
                            endforeach;


                        endif;
                    ?>

                    <!-- post type category filter area here -->
                    <?php else: ?>
                    <?php
                        $showCategoryClass = '';
                        $categories=get_categories(
                            array( 'parent' => $wp_query->get_queried_object_id() )
                        );
                        $cc = 1;
                        if( !empty($categories) ):

                            foreach ($categories as $c) :
                            // var_dump($c);
                            // what you really want instead of var_dump is something to
                            // to create markup-- list items maybe, For example...
                            $category = $c->cat_name;
                            // prablem Here pranay error ( here tag or category and id or slug) 
                            // $showCategoryClass = "tag-".str_replace(' ', '-', $category);
                            // $showCategoryClass = "category-".str_replace(' ', '-', $category);
                                    $showCategoryClass = "category-".$c->term_id;
                                    $showCategoryClassTwo = "category-".$c->slug;
                            // $showCategoryClass = "category-".$c->term_id;
                            ?>
                        <div class="toggle_out second" <?php if( $cc === 1 ){ echo 'id="toggle_first"'; } ?>>
                            <div id="toggles">
                                <!-- <input onclick="handleClick(this);" name="checkbox1" id="checkbox1" class="ios-toggle" checked="" type="checkbox">
                                <label for="checkbox1" class="checkbox-label" data-off="Λευκωσία" data-on="Λευκωσία"></label> -->

                                <input type="checkbox" name="checkbox<?php echo $cc; ?>" id="checkbox<?php echo $cc; ?>" class="ios-toggle" data-target="<?php echo $showCategoryClass; ?>" data-targettwo="<?php echo $showCategoryClassTwo; ?>"  checked />

                                <label for="checkbox<?php echo $cc; ?>" class="checkbox-label check-box-change" data-off="<?php echo $category; ?>" data-on="<?php echo $category; ?>" data-target="<?php echo $showCategoryClass; ?>"></label>
                            </div>
                        </div>
                    <?php 
                            $cc++;
                            endforeach;


                        endif;
                    endif;
                    ?>
                    
                    
                    <div style="clear:both;"></div>
                    <!-- Page contents -->
                    <!-- All JS -->
                    <!-- <script src="../assets/templates/ion/js/ion.rangeSlider.js"></script> -->
                    <!--script src="assets/templates/ion/js/ion.rangeSlider.js"></script-->
                    <!--script src="assets/templates/ion/js/moment.js"></script-->
                    <script>
                    // jQuery(function () {
                    // var $range = $("#range");
                    // $range.ionRangeSlider({
                    // hide_min_max: true,
                    // keyboard: true,
                    // step: 500,
                    // force_edges: true,
                    // grid_num: 12,
                    // grid: true,
                    // type: "double",
                    // values: [
                    
                    // "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος",
                    // "Νοέμβριος", "Δεκέμβριος", "Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάϊος", "Ιούνιος", "Ιούλιος"
                    // ],
                    // });
                    
                    
                    // $range.on("change", function () {
                    // var $this = $(this),
                    // from = $this.data("from"),
                    // to = $this.data("to");
                    // //alert(from + " - " + to);
                    // if ((from > '8') || (to < '8')){
                    // jQuery(".April").hide();
                    // }
                    // else {
                    // jQuery(".April").show('slow');
                    // }
                    // if ((from > '7') || (to < '7')){
                    // jQuery(".March").hide();
                    // }
                    // else {
                    // jQuery(".March").show('slow');
                    // }
                    // if ((from > '6') || (to < '6')) {
                    // jQuery(".February").hide();
                    // }
                    // else {
                    // jQuery(".February").show('slow');
                    // }
                    // if ((from > '5') || (to < '5')) {
                    // jQuery(".January").hide();
                    // }
                    // else {
                    // jQuery(".January").show('slow');
                    // }
                    // if ((from > '4') || (to < '4')) {
                    // jQuery(".December").hide();
                    // }
                    // else {
                    // jQuery(".December").show('slow');
                    // }
                    // if ((from > '3') || (to < '3')) {
                    // jQuery(".November").hide();
                    // }
                    // else {
                    // jQuery(".November").show('slow');
                    // }
                    // if ((from > '2') || (to < '2')) {
                    // jQuery(".October").hide();
                    // }
                    // else {
                    // jQuery(".October").show('slow');
                    // }
                    // if ((from > '1') || (to < '1')) {
                    // jQuery(".September").hide();
                    // }
                    // else {
                    // jQuery(".September").show('slow');
                    // }
                    // if ((from > '0') || (to < '0')) {
                    // jQuery(".August").hide();
                    // }
                    // else {
                    // jQuery(".August").show('slow');
                    // }
                    // if ((from > '11') || (to < '11')) {
                    // jQuery(".July").hide();
                    // }
                    // else {
                    // jQuery(".July").show('slow');
                    // }
                    // if ((from > '10') || (to < '10')) {
                    // jQuery(".June").hide();
                    // }
                    // else {
                    // jQuery(".June").show('slow');
                    // }
                    // if ((from > '9') || (to < '9')) {
                    // jQuery(".May").hide();
                    // }
                    // else {
                    // jQuery(".May").show('slow');
                    // }
                    // });
                    
                    
                    // });
                    </script>
                </div>
                <div class="hide_mobile"> <br/><br/></div>
                <div class="homeSectionWhatSay clearfix">

                    <?php while (have_posts()): the_post(); ?>
                       
                    <div <?php post_class(); ?>>
                        <div class="postBox" id="div-famagusta">
                            <a class="postBox-image zoomin" href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'post-thumbnail', array('width'=>'620') ) ?>
                            </a>
                            <div class="postBox-content">
                                <div class="contentTop clearfix">
                                    
                                    
                                    <span class="postPublishedDate">
                                        <b>Άνοιξε <?php the_time('d/m/Y'); ?></b>
                                    </span>
                                </div>
                                <div class="postTitle">
                                    <a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php endwhile; ?>

                    
                </div>
            </div>
        </section>
        

<?php
get_footer();
