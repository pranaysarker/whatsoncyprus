<?php
/**
* The template for displaying all single posts
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
*
* @package whatsnewcyprus
*/
get_header();
?>
<section id="copy" style="max-width:1150px;margin:auto;">
  <article id="listing">
    <?php while (have_posts()): the_post(); ?>
    <div id="hero" class="zoomin" style="max-width:800px;overflow:hidden;">
      <?php the_post_thumbnail( 'full', array('width'=>'800','height'=>'auto') ); ?>
    </div>
    
    <h1><?php the_title(); ?></h1>
    <div class="opened">
      <span>Άνοιξε - <?php the_time( 'd/m/Y' ); ?></span>
    </div>
    
    
    <div class="wpEditorContent" style="clear:left;">
      <div class="content_img">
        <?php the_content(); ?>
      </div>
    </div>
    <br>
  <?php endwhile; ?>
<br>
<div style="clear:both;"></div>
<br>
<div>
<?php
    $id = get_the_id();
    $terms = get_the_terms( $id, 'category' );
?>
<h1>
  <a style="text-decoration:none;color:#000000;" href="<?php echo get_term_link( $terms[0],'category'); ?>">Δείτε ακόμη...</a>
</h1>
  <?php
  global $post;
  $orig_post = $post;
  $tags = wp_get_post_tags($post->ID);
   
  if ($tags) {
    $tag_ids = array();
    foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
    $args=array(
      'tag__in' => $tag_ids,
      'post__not_in' => array($post->ID),
      'posts_per_page'=>4, // Number of related posts to display.
      'caller_get_posts'=>1
    );
  }
   
  $my_query = new wp_query( $args );
 
  while( $my_query->have_posts() ) : $my_query->the_post();
  ?>
<div class="postBox max-height">
  <a class="zoomin" href="<?php the_permalink(); ?>">
  <?php the_post_thumbnail( $size = 'post-thumbnail', $attr = '' ); ?>
  </a>
  <div class="postBox-content" style="position:inherit;">
  <span class="postPublishedDate">
  <b>Άνοιξε <?php the_time('d/m/Y'); ?></b>
  </span>
  <div class="postTitle">
  <a href="<?php the_permalink(); ?>"> Caffè Nero (Δάλι)</a>
  </div>
  </div>
</div>
<?php endwhile; ?>

<h1><a style="text-decoration:none;color:#000000;" href="<?php echo get_category_link( $terms[0] ); ?>">Δές τα όλα...</a></h1>

  <div class="show_mobile"><hr><br></div>
</div>
</article>
<section id="sidebar" class="square">
<div id="facts" class="hide_mobile" style="color:#5f5f5f;max-width: 200px;padding:10px;border:1px solid #85af4b;">
<?php 
  $argument = new WP_Query(array(
    'post_type'       => 'post',
    'posts_per_page'  => 1
  ));
while ( have_posts() ) : the_post();  ?>
<div class="title" style="word-wrap: break-word;"><b>TA ΓΕΓΟΝΟΤΑ</b></div>
<strong>Όνομα επιχείρησης:</strong> <span><h3 style="font-size:1em;"><?php the_title(); ?></h3></span><br>
<?php if( !empty(get_post_meta( $post->ID, '_themes_category', true)) ) : ?>
<strong><b>Κατηγορία:</b></strong> <span><?php echo get_post_meta( $post->ID, '_themes_category', true); ?></span><br>
<?php endif; ?>
<?php if( !empty(get_post_meta( $post->ID, '_themes_city', true)) ) : ?>
<strong><b>Πόλη:</b></strong> <span><?php echo get_post_meta( $post->ID, '_themes_city', true); ?> </span><br>
<?php endif; ?>
<strong>Άνοιξε:</strong> <?php the_time('d/m/Y'); ?><br>
<?php if( !empty(get_post_meta( $post->ID, '_themes_address', true)) ) : ?>
<strong><b>Διεύθυνση:</b></strong> <?php echo get_post_meta( $post->ID, '_themes_address', true); ?><br>
<?php endif; ?>
<?php if( !empty(get_post_meta( $post->ID, '_themes_phone', true)) ) : ?>
<strong><b>Τηλέφωνο:</b></strong> <span><?php echo get_post_meta( $post->ID, '_themes_phone', true); ?></span><br>
<?php endif; ?>
<?php if( !empty(get_post_meta( $post->ID, '_themes_working_hour', true)) ) : ?>
<strong><b>'Ωρες λειτουργίας:</b></strong> <?php echo get_post_meta( $post->ID, '_themes_working_hour', true); ?><br>
<?php endif; ?>
<?php if( !empty(get_post_meta( $post->ID, '_themes_website', true)) ) : ?>
<strong>Website:</strong> <a style="text-decoration:none;" href="<?php echo get_post_meta( $post->ID, '_themes_website', true); ?>" target="_blank">Visit Facebook</a><br>
<?php endif; ?>
<?php if( !empty(get_post_meta( $post->ID, '_themes_facebook', true)) ) : ?>
<strong>Facebook:</strong> <a style="text-decoration:none;" href="<?php echo get_post_meta( $post->ID, '_themes_facebook', true); ?>" target="_blank">Visit Website</a><br>
<?php endif; ?>
<?php if( !empty(get_post_meta( $post->ID, '_themes_instagram', true)) ) : ?>
<strong>Instagram:</strong> <a style="text-decoration:none;" href="<?php echo get_post_meta( $post->ID, '_themes_instagram', true); ?>" target="_blank">Visit Instagram</a><br>
<?php endif; ?>
<?php if( !empty(get_post_meta( $post->ID, '_themes_video_link', true)) ) : ?>
<strong>Video Link:</strong> <a style="text-decoration:none;" href="<?php echo get_post_meta( $post->ID, '_themes_video_link', true); ?>" target="_blank">Visit Video</a><br>
<?php endif; ?>
<?php if( !empty(get_post_meta( $post->ID, '_themes_twitter', true)) ) : ?>
<strong>Twitter Link:</strong> <a style="text-decoration:none;" href="<?php echo get_post_meta( $post->ID, '_themes_twitter', true); ?>" target="_blank">Visit Twitter</a><br>
<?php endif; ?>
<?php endwhile; ?>

<div style="clear:both;"></div>
</div>
<div>
</div>
</section>
<div class="show_mobile" style="margin: 20px;">
</div>
</section>
<?php get_footer(); ?>