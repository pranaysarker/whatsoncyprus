jQuery(function () {
        var $range = $("#range");
        $range.ionRangeSlider({
            hide_min_max: true,
            keyboard: true,
            step: 500,
            force_edges: true,
            grid_num: 12,
            grid: true,
            type: "double",
            values: [
                
                "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος",
                "Νοέμβριος", "Δεκέμβριος", "Ιανουάριος", "Φεβρουάριος", "Μάρτιος", "Απρίλιος", "Μάϊος", "Ιούνιος", "Ιούλιος"
            ],
        });
         
        
        $range.on("change", function () {
            var $this = $(this),
                from = $this.data("from"),
                to = $this.data("to");
                //alert(from + " - " + to);
                if ((from > '8') || (to < '8')){
                    $(".April").hide();
                }
                else {
                    $(".April").show('slow');
                }
                if ((from > '7') || (to < '7')){
                    $(".March").hide();
                }
                else {
                    $(".March").show('slow');
                }
                if ((from > '6') || (to < '6')) {
                    $(".February").hide();
                }
                else {
                    $(".February").show('slow');
                }
                if ((from > '5') || (to < '5')) {
                    $(".January").hide();
                }
                else {
                    $(".January").show('slow');
                }
                if ((from > '4') || (to < '4')) {
                    $(".December").hide();
                }
                else {
                    $(".December").show('slow');
                }
                if ((from > '3') || (to < '3')) {
                    $(".November").hide();
                }
                else {
                    $(".November").show('slow');
                }
                if ((from > '2') || (to < '2')) {
                    $(".October").hide();
                }
                else {
                    $(".October").show('slow');
                }
                if ((from > '1') || (to < '1')) {
                    $(".September").hide();
                }
                else {
                    $(".September").show('slow');
                }
                if ((from > '0') || (to < '0')) {
                    $(".August").hide();
                }
                else {
                    $(".August").show('slow');
                }
                if ((from > '11') || (to < '11')) {
                    $(".July").hide();
                }
                else {
                    $(".July").show('slow');
                }
                if ((from > '10') || (to < '10')) {
                    $(".June").hide();
                }
                else {
                    $(".June").show('slow');
                }
                if ((from > '9') || (to < '9')) {
                    $(".May").hide();
                }
                else {
                    $(".May").show('slow');
                }
        }); 
